#!/bin/bash

# this is called when Openshift wants Jenkins to shut down and provides an opportunity to
# shut down gracefully.
# This is only possible if users generate a Jenkins API token as we can't do this automatically
# anymore following https://jenkins.io/blog/2018/07/02/new-api-token-system/
if [[ -n "${JENKINS_API_TOKEN}" ]]; then
    curl -X POST --fail -u admin:${JENKINS_API_TOKEN} http://localhost:${JENKINS_SERVICE_PORT_WEB}/safeExit && sleep infinity
fi
