#!/bin/bash

set -e

# wait until the Jenkins instance starts successfully or TIMEOUT_IN_MINUTES have elapsed
TIMEOUT_IN_MINUTES=${TIMEOUT_IN_MINUTES:-10}

# break on error messages in Jenkins startup log if value is YES
BREAK_ON_ERRORS=${BREAK_ON_ERRORS:-YES}

# print log if value is YES
PRINT_STARTUP_LOG=${PRINT_STARTUP_LOG:-YES}

if [ "${PRINT_STARTUP_LOG}" == "YES" ]; then
  trap 'echo "Jenkins startup log dump:"; echo "${log}"' EXIT
fi

STOP_AT=$[$SECONDS + $TIMEOUT_IN_MINUTES * 60]
SLEEP_INTERVAL=${SLEEP_INTERVAL:-30}

while [ "${SECONDS}" -lt "${STOP_AT}" ]; do
  # Deployment usually takes a couple minutes.
  # If we're running just after a redeployment command then we might not immediately see the latest deployment
  # so wait first before polling for deployment logs.
  sleep ${SLEEP_INTERVAL}s;
  log=$(oc logs dc/jenkins || true)
  if [ "${BREAK_ON_ERRORS}" == "YES" ]; then
    # look for errors in log
    echo "{$log}" | grep -q 'SEVERE' && exit 1
  fi

  # check for startup success
  echo "{$log}" | grep -q 'Jenkins is fully up and running' && exit 0

  echo "Jenkins instance is not ready yet, trying again in ${SLEEP_INTERVAL} seconds...";
done

echo "Failure to get a running Jenkins instance in ${TIMEOUT_IN_MINUTES} minutes"
exit 1
