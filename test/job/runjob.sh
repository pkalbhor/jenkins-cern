#!/bin/bash
set -e

job_name='simpleJob'

TIMEOUT_IN_MINUTES=${TIMEOUT_IN_MINUTES:-10}

# get credentials for accessing the Jenkins API
hostname=$(oc get route/jenkins -o go-template --template '{{.spec.host}}')
url="https://admin:${JENKINS_API_TOKEN}@${hostname}"

# create a simple job in the Jenkins instance in current Openshift project
# cf. https://support.cloudbees.com/hc/en-us/articles/220857567-How-to-create-a-job-using-the-REST-API-and-cURL
echo "Creating a job"
curl -s --fail -XPOST "${url}/createItem?name=${job_name}" --data-binary @./test/job/simplejob.xml -H "Content-Type:text/xml"

# trigger job execution
echo "Triggering job execution"
curl -s --fail -X POST "${url}/job/${job_name}/build"

# wait until job success or timeout
STOP_AT=$[$SECONDS + $TIMEOUT_IN_MINUTES * 60]
while [ "${SECONDS}" -lt "${STOP_AT}" ]; do
  result=$(curl -s --fail "${url}/job/${job_name}/lastBuild/api/json" | jq -r .result)
  if [ "${result}" == "" -o "${result}" == "null" ]; then
    echo "Job not complete yet. Waiting 30s"
    sleep 30
  elif [ "${result}" == "SUCCESS" ]; then
    echo "Job success!"
    exit 0
  else
    echo "Job result: '${result}'."
    exit 1
  fi
done

echo "Failure to get job success in ${TIMEOUT_IN_MINUTES} minutes"
exit 1
